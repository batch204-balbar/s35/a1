/*
	Mini-Activity: 6:05PM
		Create an expressJS server designated to port 4000
		Create a new route with endpoint /hello and method GET
			-Should be able to respond with "Hello Word"

*/

const express = require("express");

//Mongoose is a package that allows creation of Schemas to model our data structures
//Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();

const port = 4000;

//MongoDB Connection
// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values

/*
	Syntax:
		mongoose.connect("<MongoDB Atlas Connection String>")

*/

mongoose.connect("mongodb+srv://angelobalbar:admin123@cluster0.q4we0.mongodb.net/B204-to-dos?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);


//Set notifications for connection success or failure
// Connection to the database
// Allows to handle errors when the initial connection is establised
let db = mongoose.connection;
//If a connection error occured, output in the console
db.on("error", console.error.bind(console, "Connection Error"));

//If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database."));


//Mongoose Schema
//Schemas determine the structure of the documents to be written in the database
//Schemas acts as blueprints to our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});


//Models
// Uses schemas and are used to create/instantiate objects that correspond to the schema
// Models use Schemas and they act as the middleman from the server (JS code) to our database

//The first parameter of the Mongoose model method indicates the collection in where to store the data
//The second parameter is used to specify the schema/blueprint of the documents that will be stored in the MongoDB collection
const Task = mongoose.model("Task", taskSchema);


app.use(express.json());

app.get("/hello", (req, res) => {
	res.send("Hello World!");
});

// Route to create a task
app.post("/tasks", (req, res) => {
	console.log(req.body)
	Task.findOne({name: req.body.name}, (err, result) => {
			if (result !== null && result.name == req.body.name) {
				return res.send("Duplicate Task Found");
			} else {
				let newTask = new Task({
					name: req.body.name
				});
				newTask.save((saveErr, savedTask) => {
					if(saveErr) {
						return console.error(saveErr)
					} else {
						return res.status(201).send ("New Task created");
					}
				})
			}
	});
})
// Get all the tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})

// Activity POST route signup
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

const User = mongoose.model("User", userSchema);
app.post("/signup", (req, res) => {
	console.log(req.body);
	User.findOne({username: req.body.username}, (err, result) => {
			if(result != null && result.username == req.body.username){
			return res.send("Username not available");

			} else {
				if(req.body.username !== "" && req.body.password !== ""){

                let newUser = new User({
                    username : req.body.username,
                    password : req.body.password
                });

                newUser.save((saveErr, savedTask) => {

                    if(saveErr){
                        return console.error(saveErr);

                    } else {
                        return res.status(201).send("New User Registered");
                    }
                })

            } else {

                return res.send("Please input Username and Password");
            }			
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`));
